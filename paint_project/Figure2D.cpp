/// <summary>
/// file	Figure2D.cpp
/// 
/// author	Shahar Azar		208427302
/// author	Aviad Partoosh	313548323
/// date	September 2020
/// </summary>

#include "Figure2D.h"

/***/
IMPLEMENT_SERIAL(EllipseF, Figure2D, 1)
IMPLEMENT_SERIAL(RectangleF, Figure2D, 1)
IMPLEMENT_SERIAL(SegmentF, Figure2D, 1)
IMPLEMENT_SERIAL(TriangleF, Figure2D, 1)
IMPLEMENT_SERIAL(R_A_Triangle, TriangleF, 1)
IMPLEMENT_SERIAL(S_O_David, TriangleF, 1)

/********************Constructors and deconstrutors********************/
Figure2D::Figure2D() {};
Figure2D::Figure2D(int x1, int y1, int x2, int y2, COLORREF insideColor, COLORREF outsideColor, int frameSize)
{
	this->p1.x = x1;
	this->p2.x = x2;
	this->p1.y = y1;
	this->p2.y = y2;
	this->insideColor = insideColor;
	this->outsideColor = outsideColor;
	this->frameSize = frameSize;
}

bool Figure2D::IsInside(const CPoint & P) const
{
	return (p1.x <= P.x && P.x <= p2.x || p1.x >= P.x && P.x >= p2.x) &&
		(p1.y <= P.y && P.y <= p2.y || p1.y >= P.y && P.y >= p2.y);
}

Figure2D::~Figure2D() {}


EllipseF::EllipseF() : Figure2D() {};
EllipseF::EllipseF(int x1, int y1, int x2, int y2, COLORREF insideColor, COLORREF outsideColor, int frameSize) :
	Figure2D(x1, y1, x2, y2, insideColor, outsideColor, frameSize)
{
	KIND = 1;
};

RectangleF::RectangleF() : Figure2D() {};
RectangleF::RectangleF(int x1, int y1, int x2, int y2, COLORREF insideColor, COLORREF outsideColor, int frameSize) :
	Figure2D(x1, y1, x2, y2, insideColor, outsideColor, frameSize)
{
	this->KIND = 2;
	this->insideColor = insideColor;
	this->outsideColor = outsideColor;
	this->frameSize = frameSize;
};

SegmentF::SegmentF() : Figure2D() {};
SegmentF::SegmentF(int x1, int y1, int x2, int y2, COLORREF insideColor, COLORREF outsideColor, int frameSize) :
	Figure2D(x1, y1, x2, y2, insideColor, outsideColor, frameSize)
{
	KIND = 3;
};

TriangleF::TriangleF() : Figure2D() {};
TriangleF::TriangleF(int x1, int y1, int x2, int y2, COLORREF insideColor, COLORREF outsideColor, int frameSize) :
	Figure2D(x1, y1, x2, y2, insideColor, outsideColor, frameSize)
	
{
	KIND = 4;
};

void TriangleF::Redefine(CPoint p1, CPoint p2)
{
	this->p1 = p1;
	this->p2 = p2;
	int x1 = p1.x, y1 = p1.y, 
		x2 = p2.x, y2 = p2.y;
	if (x2 > x1)
	{
		// [0] => bottom left, [1] => bottom right, [2] => top
		T_P[0].x = x1;
		T_P[0].y = y2;
		T_P[1].x = x2;
		T_P[1].y = y2;
		T_P[2].x = (((T_P[1].x - T_P[0].x) / 2) + T_P[0].x);
		T_P[2].y = y1;
	}
	else
	{ // [0] => bottom rigth, [1] => left bottom, [2] => top
		T_P[0].x = x1;
		T_P[0].y = y2;
		T_P[1].x = x2;
		T_P[1].y = y2;
		T_P[2].x = (((T_P[0].x - T_P[1].x) / 2) + T_P[1].x);
		T_P[2].y = y1;
	}
}

R_A_Triangle::R_A_Triangle() : TriangleF() {};
R_A_Triangle::R_A_Triangle(int x1, int y1, int x2, int y2, COLORREF insideColor, COLORREF outsideColor, int frameSize) :
	TriangleF(x1, y1, x2, y2, insideColor, outsideColor, frameSize)

{

	KIND = 5;
};

void R_A_Triangle::Redefine(CPoint p1, CPoint p2)
{
	this->p1 = p1;
	this->p2 = p2;
	int x1 = p1.x, y1 = p1.y,
		x2 = p2.x, y2 = p2.y;
	if (x2 > x1)
	{
		// [0] => top left, [1] => bottom right, [2] => botton left
		T_P[0].x = x1;
		T_P[0].y = y1;
		T_P[1].x = x2;
		T_P[1].y = y2;
		T_P[2].x = x1;
		T_P[2].y = y2;
	}
	else
	{ // [0] => bottom rigth, [1] => left bottom, [2] => top
		T_P[0].x = x1;
		T_P[0].y = y2;
		T_P[1].x = x2;
		T_P[1].y = y2;
		T_P[2].x = x2;
		T_P[2].y = y1;
	}
}

S_O_David::S_O_David() : TriangleF() {};
S_O_David::S_O_David(int x1, int y1, int x2, int y2, COLORREF insideColor, COLORREF outsideColor, int frameSize) :
	TriangleF(x1, y1, x2, y2, insideColor, outsideColor, frameSize)

{

	KIND = 6;
};

void S_O_David::Redefine(CPoint p1, CPoint p2)
{
	this->p1 = p1;
	this->p2 = p2;
	int dist = sqrt(pow(p1.y - p2.y, 2));
	if (p1.y < p2.y)
	{
		T_P[0] = p1;
		T_P[1] = { (p1.x + p2.x) / 2, p2.y };
		T_P[2] = { p2.x, p1.y };

		T_Q[0] = { p1.x, p1.y + ((2 * dist) / 3) };
		T_Q[1] = { p2.x, p1.y + ((2 * dist) / 3) };
		T_Q[2] = { T_P[1].x,p1.y - (dist / 3) };
	}
	else
	{
		T_P[0] = p1;
		T_P[1] = { (p1.x + p2.x) / 2, p2.y };
		T_P[2] = { p2.x, p1.y };

		T_Q[0] = { T_P[1].x,p1.y + (dist / 3) };
		T_Q[1] = { p1.x, p2.y + ( dist / 3) };
		T_Q[2] = { p2.x, p2.y + (dist / 3) };
	}

}






