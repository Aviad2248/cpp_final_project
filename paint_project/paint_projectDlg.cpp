// paint_projectDlg.cpp : implementation file
//

#include "stdafx.h"
#include "paint_project.h"
#include "paint_projectDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

														// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CpaintprojectDlg dialog



CpaintprojectDlg::CpaintprojectDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_PAINT_PROJECT_DIALOG, pParent),
	m_pToolsButtonArr()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	futureFigKIND = ellipse;
	f = NULL;
	fileName = "";
	m_whitebrush = NULL;
	indexCurr = 0;
	bitmap = -1;
	chosenWidth = DEFAULT_WIDTH;
	lButtonDown = false;
	touched = false;
	saved = false;
	isFirstColor = true;
	isBgChanged = true;
	firstColor = RGB(0, 0, 0);
	secondColor = RGB(255, 255, 255);
	userAction = noAction;
	topDrawingPoint.x = 0;
	topDrawingPoint.y = 85;
	bottomDrawingPoint.x = 1020;
	bottomDrawingPoint.y = 685;
}
CpaintprojectDlg::~CpaintprojectDlg()
{
	for (int i = 0; i < indexCurr; i++)
		delete &(figs[i]);
}
void CpaintprojectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COLORPICKER1, ConColorButt1);
	DDX_Control(pDX, IDC_COLORPICKER2, ConColorButt2);
	DDX_Control(pDX, IDC_SLIDER1, WidthSlider);
	DDX_Control(pDX, IDC_CHOICE, m_Choice);
}

BEGIN_MESSAGE_MAP(CpaintprojectDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_COMMAND(ID_MENU_NEW, &CpaintprojectDlg::onMenuNew)
	ON_COMMAND(ID_MENU_OPEN, &CpaintprojectDlg::OnMenuOpen)
	ON_COMMAND(ID_MENU_SAVE, &CpaintprojectDlg::OnMenuSave)
	ON_COMMAND(ID_MENU_SAVEAS, &CpaintprojectDlg::OnMenuSaveas)
	ON_COMMAND(ID_MENU_ABOUT, &CpaintprojectDlg::OnMenuAbout)
	ON_COMMAND(ID_MENU_EXITPAINT, &CpaintprojectDlg::OnMenuExitpaint)
	ON_COMMAND_RANGE(IDI_ERASER, IDI_TRASH, &CpaintprojectDlg::OnToolButtonClick)
	ON_COMMAND_RANGE(IDI_ELLIPSE, IDI_STAROFDAVID, &CpaintprojectDlg::OnFigureButtonClick)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_ERASEBKGND()
	ON_BN_CLICKED(IDC_COLOR1RADIO, &CpaintprojectDlg::OnBnClickedColor1radio)
	ON_BN_CLICKED(IDC_COLOR2RADIO, &CpaintprojectDlg::OnBnClickedColor2radio)
	ON_BN_CLICKED(IDC_COLORPICKER1, &CpaintprojectDlg::OnBnClickedColorpicker1)
	ON_BN_CLICKED(IDC_COLORPICKER2, &CpaintprojectDlg::OnBnClickedColorpicker2)
	ON_WM_HSCROLL()
	ON_CBN_SELCHANGE(IDC_CHOICE, &CpaintprojectDlg::OnCbnSelchangeCombo1)
	ON_BN_CLICKED(IDC_UNDO, &CpaintprojectDlg::OnBnClickedUndo)
	ON_BN_CLICKED(IDC_REDO, &CpaintprojectDlg::OnBnClickedRedo)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CpaintprojectDlg message handlers

BOOL CpaintprojectDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	// Add the system menu
	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	HICON hIcon = LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON1));
	SetIcon(hIcon, TRUE);		// Set big icon
	SetIcon(hIcon, FALSE);		// Set small icon

	CheckRadioButton(IDC_COLOR1RADIO, IDC_COLOR2RADIO, IDC_COLOR1RADIO);

	CreateToolButtons(); // Add the tool buttons 
	CreateFigureButtons(); // Add the figure buttons
	DefineColorButtons(); // Define color palletes and radio buttons
	DefineWidthSlider(); // Define the width slider

						 // Set default selected "empty board"
	m_Choice.SetCurSel(0);

	// Set undo and redo amount to 0 
	UpdateOnScreenUndoAmount(0);
	UpdateOnScreenRedoAmount(0);



	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CpaintprojectDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CpaintprojectDlg::OnPaint()
{

	CPaintDC dc(this); // device context for painting
	if (IsIconic())
	{
		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	{
		// Draw the default grey color for toolbar
		COLORREF grey = RGB(240, 240, 240);
		CPen pen(PS_SOLID, 1, grey); // Pen <=> frame
		CPen* old = dc.SelectObject(&pen); // for figures
		CBrush brush(grey); // Brush <=> fill
		CBrush* oldB = dc.SelectObject(&brush);
		dc.Rectangle(0, 0, 1020, 85);

		// Draw the background
		RedrawBackground(&dc);

		// Draw figures
		RedrawAllFigs(&dc);

		// Draw lines
		RedrawAllLines(&dc);

		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CpaintprojectDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

/// <summary>
/// Create the tool buttons dynamically and add handle
/// </summary>
void CpaintprojectDlg::CreateToolButtons()
{
	int icons[] = {
		IDI_PENCIL,
		IDI_PAINT_BUCKET,
		//IDI_TEXT,
		IDI_ERASER,
		IDI_SAVE,
		IDI_TRASH };
	char* toolTips[] = {
		"Pencil \nDraw a free-form line with the selected line width.",
		"Fill with color \nClick an area on the canvas to fill it with the foreground color, or right-click to fill it with the background color.",
		//"Text \nInsert text into the picture.",
		"Eraser \nErase part of the picture and replace it with the background color.",
		"Save \nSave the current drawing.",
		"Delete \nDelete the current drawing."
	};
	int startingX = 15, startingY = 15;
	int buttonSize = 32;
	int buttonsPerLine = 3;
	int x = startingX, y = startingY, r = startingX + buttonSize, b = startingY + buttonSize; // Point(x,y) => Top left corner, Point(r,b) Bottom right corner

	for (int i = 0;i < sizeof(icons) / sizeof(icons[0]);i++)
	{
		m_pToolsButtonArr[i] = new CMFCButton;
		// Create an icon button.
		m_pToolsButtonArr[i]->Create(_T(""), WS_CHILD | WS_VISIBLE | BS_ICON | BS_BITMAP,
									 CRect(x, y, r, b), this, 1);

		// Add icon to the button 
		HICON myIcon = static_cast<HICON>(::LoadImage(GetModuleHandle(NULL),
													  MAKEINTRESOURCE(icons[i]),
													  IMAGE_ICON,
													  0, 0,    // or whatever size icon you want to load
													  0));
		m_pToolsButtonArr[i]->SetIcon(myIcon);

		// Set tooltip
		m_pToolsButtonArr[i]->SetTooltip(CA2W(toolTips[i])); // Add text
		(m_pToolsButtonArr[i]->GetToolTipCtrl()).SetMaxTipWidth(SHRT_MAX); // Enables multiline


																		   // Set button ID as the resource id
		m_pToolsButtonArr[i]->SetDlgCtrlID(icons[i]);

		// Increase buttons P1/P2, 3 per row 
		x += 32;
		y += (((i + 1) % buttonsPerLine == 0) && ((i + 1) != 0)) ? buttonSize : 0;
		r += 32;
		b += (((i + 1) % buttonsPerLine == 0) && ((i + 1) != 0)) ? buttonSize : 0;

		x = (((i + 1) % buttonsPerLine == 0) && ((i + 1) != 0)) ? startingX : x;
		r = (((i + 1) % buttonsPerLine == 0) && ((i + 1) != 0)) ? startingX + buttonSize : r;
	}
}

void CpaintprojectDlg::CreateFigureButtons()
{
	int icons[] = { IDI_RECTANGLE, IDI_ELLIPSE, IDI_SEGMENT, IDI_ISOSCELESTRIANGLE, IDI_RIGHTTRIANLGE, IDI_STAROFDAVID };
	char* toolTips[] = {
		"Rectangle \nA quadrilateral with four right angles.",
		"Ellipse \nA plane curve surrounding two focal points, such that for all points on the curve, the sum of the two distances to the focal points is a constant.",
		"Line segment \nA straight line between two points",
		"Isosceles triangle \nA trianlge with (at least) two equal sides.",
		"Right trianlge \nA triangle with an angle of 90°.",
		"Star of David \nTwo overlaid equilateral triangles that form a six-pointed star."
	};
	int startingX = 670, startingY = 15;
	int buttonSize = 32;
	int buttonsPerLine = 6;
	int x = startingX, y = startingY, r = startingX + buttonSize, b = startingY + buttonSize; // Point(x,y) => Top left corner, Point(r,b) Bottom right corner

	for (int i = 0;i < sizeof(icons) / sizeof(icons[0]);i++)
	{
		m_pToolsButtonArr[i] = new CMFCButton;
		// Create an icon button.
		m_pToolsButtonArr[i]->Create(_T(""), WS_CHILD | WS_VISIBLE | BS_ICON | BS_BITMAP,
									 CRect(x, y, r, b), this, 1);

		// Add icon to the button 
		HICON myIcon = static_cast<HICON>(::LoadImage(GetModuleHandle(NULL),
													  MAKEINTRESOURCE(icons[i]),
													  IMAGE_ICON,
													  0, 0,    // or whatever size icon you want to load
													  0));
		m_pToolsButtonArr[i]->SetIcon(myIcon);

		// Set tooltip
		m_pToolsButtonArr[i]->SetTooltip(CA2W(toolTips[i])); // Add text
		(m_pToolsButtonArr[i]->GetToolTipCtrl()).SetMaxTipWidth(SHRT_MAX); // Enables multiline


																		   // Set button ID as the resource id
		m_pToolsButtonArr[i]->SetDlgCtrlID(icons[i]);

		// Increase buttons P1/P2, 3 per row 
		x += 32;
		y += (((i + 1) % buttonsPerLine == 0) && ((i + 1) != 0)) ? buttonSize : 0;
		r += 32;
		b += (((i + 1) % buttonsPerLine == 0) && ((i + 1) != 0)) ? buttonSize : 0;

		x = (((i + 1) % buttonsPerLine == 0) && ((i + 1) != 0)) ? startingX : x;
		r = (((i + 1) % buttonsPerLine == 0) && ((i + 1) != 0)) ? startingX + buttonSize : r;
	}
}

/// <summary>
/// Dynamic tool button handle
/// </summary>
/// <param name="id"></param>
void CpaintprojectDlg::OnToolButtonClick(UINT id)
{
	Action newAction;

	switch (id)
	{
	case IDI_ERASER:
	{
		newAction = (GetUserAction() == eraser) ? noAction : eraser;
		break;
	}
	case IDI_PAINT_BUCKET:
	{
		newAction = (GetUserAction() == paintBucket) ? noAction : paintBucket;
		break;
	}
	case IDI_PENCIL:
	{
		newAction = (GetUserAction() == freeStyle) ? noAction : freeStyle;
		break;
	}
	case IDI_TEXT:
	{
		newAction = (GetUserAction() == figure) ? noAction : figure;
		break;
	}
	case IDI_TRASH:
	{
		newAction = noAction;
		// Clear screen
		if (!saved && touched)
		{
			const int saveOrClose = AfxMessageBox(_T("There seems to be some unsaved changes. \nWould you like to save before clearing the screen?"), MB_YESNO | MB_ICONQUESTION);
			if (saveOrClose == IDYES)
				PerformSave();
			ClearScreenAndResetToDefault();
		}
		else if (touched)
		{
			const int isClose = AfxMessageBox(_T("Are you sure you would like to clear the screen?"), MB_YESNO | MB_ICONWARNING);
			if (isClose == IDNO)
				break;
			ClearScreenAndResetToDefault();
		}
		break;
	}
	case IDI_SAVE:
	{
		newAction = noAction;
		PerformSave();
		break;
	}
	default:
	{
		newAction = noAction;
		MessageBox(_T("Error! No button with ID : " + id));
	}
	}

	// Double click on icon disables icon 
	if (newAction == noAction)
	{
		SetNone();
	}
	else
	{
		SetUserAction(newAction);
		SetCursor(id);
		SetDialogTitle(id);
	}


	lButtonDown = false;
}

void CpaintprojectDlg::OnFigureButtonClick(UINT id)
{
	Figure newFigure;

	switch (id)
	{
	case IDI_ELLIPSE:
	{
		newFigure = (GetCurrentFigure() == ellipse) ? noFigure : ellipse;
		break;
	}
	case IDI_RECTANGLE:
	{
		newFigure = (GetCurrentFigure() == rectangle) ? noFigure : rectangle;
		break;
	}
	case IDI_SEGMENT:
	{
		newFigure = (GetCurrentFigure() == segment) ? noFigure : segment;
		break;
	}
	case IDI_ISOSCELESTRIANGLE:
	{
		newFigure = (GetCurrentFigure() == isoscelesTriangle) ? noFigure : isoscelesTriangle;
		break;
	}
	case IDI_RIGHTTRIANLGE:
	{
		newFigure = (GetCurrentFigure() == rightTriangle) ? noFigure : rightTriangle;
		break;
	}
	case IDI_STAROFDAVID:
	{
		newFigure = (GetCurrentFigure() == starOfDavid) ? noFigure : starOfDavid;
		break;
	}
	default:
	{
		newFigure = noFigure;
		MessageBox(_T("Error! No button with ID : " + id));
	}
	}

	// Double click on icon disables icon 
	if (newFigure == noFigure)
	{
		SetNone();
	}
	else
	{
		SetUserAction(figure);
		SetCurrentFigure(newFigure);
		SetCursor(id);
		SetDialogTitle(id);
	}


	lButtonDown = false;
}

void CpaintprojectDlg::DefineColorButtons()
{
	// Color picker 1
	ConColorButt1.SetColor(RGB(0, 0, 0));		// Default starting color1 is black
	ConColorButt1.SetTooltip(CA2W("Color 1(foreground color) \nClick here to select a color. This color is used with pencil and for shape outlines."));
	(ConColorButt1.GetToolTipCtrl()).SetMaxTipWidth(SHRT_MAX); // Enables multiline

															   // Color picker 2
	ConColorButt2.SetColor(RGB(255, 255, 255));	// Default starting color2 is white
	ConColorButt2.SetTooltip(CA2W("Color 2(background color) \nClick here to select a color. This color is used with the eraser and for shape fills."));
	(ConColorButt2.GetToolTipCtrl()).SetMaxTipWidth(SHRT_MAX); // Enables multiline

}

void CpaintprojectDlg::DefineWidthSlider()
{
	// width slider
	WidthSlider.SetRange(1, 30, true); // Range is 1 <--> 30
	WidthSlider.SetPos(chosenWidth); // Starting width is 5
}


/// <summary>
/// Clear the screen when choosing the "new" option
/// </summary>
void CpaintprojectDlg::onMenuNew()
{

	if (!saved && touched)
	{
		const int saveBeforeClear = AfxMessageBox(_T("There seems to be some unsaved changes. \nWould you like to save before clearing the screen?"), MB_YESNO | MB_ICONQUESTION);
		if (saveBeforeClear == IDYES)
			PerformSave();
		// Clear screen
		ClearScreenAndResetToDefault();
	}
	else if (touched)
	{
		// Clear screen
		const int isClose = AfxMessageBox(_T("Are you sure you would like to clear the screen?"), MB_YESNO | MB_ICONWARNING);
		if (isClose == IDYES)
			ClearScreenAndResetToDefault();

	}


}

/// <summary>
/// Choosing open
/// </summary>
void CpaintprojectDlg::OnMenuOpen()
{
	if (!saved && touched)
	{
		const int saveOrClose = AfxMessageBox(_T("There seems to be some unsaved changes. \nWould you like to save before opening a new file?"), MB_YESNO | MB_ICONQUESTION);
		if (saveOrClose == IDYES)
			PerformSave();
	}

	const TCHAR czFilter[] = _T("ShaharAndAviad files (*.shaharandaviad)|*.shaharandaviad|All Files (*.*)|*.*||");
	CFileDialog fDialog(TRUE, _T("ShaharAndAviad"), NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, czFilter, this);

	if (fDialog.DoModal() == IDOK)
	{
		// Reset to default and then load the file
		ClearScreenAndResetToDefault();
		fileName = fDialog.GetPathName();
		CFile file(fileName, CFile::modeRead);
		CArchive ar(&file, CArchive::load);
		figs.Serialize(ar);
		//Update all figs
		RedfineAllFigs();
		touched = true;
		ar.Close();
		file.Close();
		Invalidate();
	}
}

/// <summary>
/// Choosing save
/// </summary>
void CpaintprojectDlg::OnMenuSave()
{
	PerformSave();
}

/// <summary>
/// Choosing save as
/// </summary>
void CpaintprojectDlg::OnMenuSaveas()
{
	PerformSaveAs();
}

/// <summary>
/// Choosing about
/// </summary>
void CpaintprojectDlg::OnMenuAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}


/// <summary>
/// Choosing exit paint
/// </summary>
void CpaintprojectDlg::OnMenuExitpaint()
{
	if (!saved && touched)
	{
		const int saveBeforeExiting = AfxMessageBox(_T("There seems to be some unsaved changes. \nWould you like to save before exiting?"), MB_YESNO | MB_ICONQUESTION);
		if (saveBeforeExiting == IDYES)
			PerformSave();
		exit(1);
	}
	else
	{
		const int isClose = AfxMessageBox(_T("Are you sure you would like to exit paint?"), MB_YESNO | MB_ICONQUESTION);
		if (isClose == IDYES)
			exit(1);
	}
}

/// <summary>
/// Getter for the userChosenColor
/// </summary>
/// <returns></returns>
COLORREF CpaintprojectDlg::GetUserChosenColor()
{
	if (IsFirstColor())
		return GetFirstColor();
	else
		return GetSecondColor();
}

void CpaintprojectDlg::SetFirstColor(COLORREF newColor)
{
	this->firstColor = newColor;
}

COLORREF CpaintprojectDlg::GetFirstColor()
{
	return this->firstColor;
}

void CpaintprojectDlg::SetSecondColor(COLORREF newColor)
{
	this->secondColor = newColor;
}

COLORREF CpaintprojectDlg::GetSecondColor()
{
	return this->secondColor;
}

/// <summary>
/// 
/// </summary>
/// <returns></returns>
bool CpaintprojectDlg::IsFirstColor()
{
	return isFirstColor;
}

void CpaintprojectDlg::SetIsFirstColor(bool newBoolVal)
{
	this->isFirstColor = newBoolVal;
}

/// <summary>
/// 
/// </summary>
/// <returns></returns>
int CpaintprojectDlg::GetUserChosenWidth()
{
	return this->chosenWidth;
}

void CpaintprojectDlg::SetUserChosenWidth(int newWidth)
{
	this->chosenWidth = newWidth;
}

/// <summary>
/// 
/// </summary>
/// <returns></returns>
Action CpaintprojectDlg::GetUserAction()
{
	return this->userAction;
}

void CpaintprojectDlg::SetCurrentFigure(Figure newFigure)
{
	this->futureFigKIND = newFigure;
}

Figure CpaintprojectDlg::GetCurrentFigure()
{
	return this->futureFigKIND;
}

/// <summary>
/// 
/// </summary>
/// <param name="action"></param>
void CpaintprojectDlg::SetUserAction(Action action)
{
	this->userAction = action;
}

/// <summary>
/// Sets a different cursor according to given id
/// </summary>
/// <param name="id">The cursor type id</param>
void CpaintprojectDlg::SetCursor(UINT id)
{
	HCURSOR Cursor;
	switch (id)
	{
	case IDI_ERASER:
		Cursor = LoadCursorFromFile(L"res\\eraser.cur");break;
	case IDI_PAINT_BUCKET:
		Cursor = LoadCursorFromFile(L"res\\paint_bucket.cur");break;
	case IDI_PENCIL:
		Cursor = LoadCursorFromFile(L"res\\pencil.cur");break;
	case IDI_TEXT:
		Cursor = LoadCursorFromFile(L"res\\text.cur");break;
		//case IDI_SAVE:
		//	Cursor = LoadCursorFromFile(L"res\\save.cur");break;
		//case IDI_TRASH:
		//	Cursor = LoadCursorFromFile(L"res\\trash.cur");break;
	case IDI_RECTANGLE:
		Cursor = LoadCursorFromFile(L"res\\rectangle.cur");break;
	case IDI_ELLIPSE:
		Cursor = LoadCursorFromFile(L"res\\ellipse.cur");break;
	case IDI_SEGMENT:
		Cursor = LoadCursorFromFile(L"res\\segment.cur");break;
	case IDI_ISOSCELESTRIANGLE:
		Cursor = LoadCursorFromFile(L"res\\isosceles_triangle.cur");break;
	case IDI_RIGHTTRIANLGE:
		Cursor = LoadCursorFromFile(L"res\\right_triangle.cur");break;
	case IDI_STAROFDAVID:
		Cursor = LoadCursorFromFile(L"res\\star_of_david.cur");break;
	default:
		Cursor = LoadCursor(NULL, IDC_ARROW);
	}

	SetClassLongPtr(m_hWnd, GCLP_HCURSOR, reinterpret_cast<LONG_PTR>(Cursor));
}

/// <summary>
/// Sets a different dialog title according to given id
/// </summary>
/// <param name="id">The dialog title id</param>
void CpaintprojectDlg::SetDialogTitle(UINT id)
{
	char* newTitle;
	switch (id)
	{
	case IDI_ERASER:
		newTitle = "Paint - Erase";break;
	case IDI_PAINT_BUCKET:
		newTitle = "Paint - Fill";break;
	case IDI_PENCIL:
		newTitle = "Paint - Draw";break;
	case IDI_TEXT:
		newTitle = "Paint - Text";break;
		//case IDI_SAVE:
		//	newTitle = "Paint - Save";break;
		//case IDI_TRASH:
		//	newTitle = "Paint - Color picker";break;
	case IDI_RECTANGLE:
		newTitle = "Paint - Rectangle";break;
	case IDI_ELLIPSE:
		newTitle = "Paint - Ellipse";break;
	case IDI_SEGMENT:
		newTitle = "Paint - Line segment";break;
	case IDI_ISOSCELESTRIANGLE:
		newTitle = "Paint - Isosceles triangle";break;
	case IDI_RIGHTTRIANLGE:
		newTitle = "Paint - Right triangle";break;
	case IDI_STAROFDAVID:
		newTitle = "Paint - Star of David";break;
	default:
		newTitle = "Paint";
	}
	SetWindowText(CA2W(newTitle));
}

/// <summary>
/// Default on left mouse button down
/// </summary>
/// <param name="nFlags"></param>
/// <param name="point"></param>
void CpaintprojectDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	//CString str1;
	//str1.Format(_T("X: %d | Y: %d"), point.x, point.y);
	//MessageBox(NULL, str1);
	// Our buttons end at Y= 81. X at 450 at the moment. 
	// Window size is 1010 X 680
	if ((point.y > topDrawingPoint.y))
	{
		//set lbutton pressed = true
		lButtonDown = true;

		//paint the line on the screen
		UpdateData(TRUE);

		int userChosenWidth = GetUserChosenWidth();
		COLORREF userChosenColor = GetUserChosenColor();

		CClientDC dc(this);
		CPen pen(PS_SOLID, userChosenWidth, userChosenColor); // Pen <=> frame
		CPen* old = dc.SelectObject(&pen);
		CBrush brush(userChosenColor); // Brush <=> fill
		CBrush* oldB = dc.SelectObject(&brush);

		switch (GetUserAction())
		{
		case noAction:
		{
			break;
		}
		case freeStyle:
		case eraser:
		{
			// Set startp as the start point
			startp = point;
			break;
		}
		case figure:
		{
			// Set startp as the start point
			startp = point;
			//add the new figure to array
			switch (GetCurrentFigure())
			{
			case ellipse:
				figs.Add(new EllipseF(startp.x, startp.y, startp.x, startp.y, GetSecondColor(), GetUserChosenColor(), chosenWidth));
				InvalidateRect(CRect(topDrawingPoint.x, topDrawingPoint.y, bottomDrawingPoint.x, bottomDrawingPoint.y));
				break;
			case rectangle:
				figs.Add(new RectangleF(startp.x, startp.y, startp.x, startp.y, GetSecondColor(), GetUserChosenColor(), chosenWidth));
				InvalidateRect(CRect(topDrawingPoint.x, topDrawingPoint.y, bottomDrawingPoint.x, bottomDrawingPoint.y));
				break;
			case segment:
				figs.Add(new SegmentF(startp.x, startp.y, startp.x, startp.y, GetSecondColor(), GetUserChosenColor(), chosenWidth));
				InvalidateRect(CRect(topDrawingPoint.x, topDrawingPoint.y, bottomDrawingPoint.x, bottomDrawingPoint.y));
				break;
			case isoscelesTriangle:
				figs.Add(new TriangleF(startp.x, startp.y, startp.x, startp.y, GetSecondColor(), GetUserChosenColor(), chosenWidth));
				InvalidateRect(CRect(topDrawingPoint.x, topDrawingPoint.y, bottomDrawingPoint.x, bottomDrawingPoint.y));
				break;
			case rightTriangle:
				figs.Add(new R_A_Triangle(startp.x, startp.y, startp.x, startp.y, GetSecondColor(), GetUserChosenColor(), chosenWidth));
				InvalidateRect(CRect(topDrawingPoint.x, topDrawingPoint.y, bottomDrawingPoint.x, bottomDrawingPoint.y));
				break;
			case starOfDavid:
				figs.Add(new S_O_David(startp.x, startp.y, startp.x, startp.y, GetSecondColor(), GetUserChosenColor(), chosenWidth));
				InvalidateRect(CRect(topDrawingPoint.x, topDrawingPoint.y, bottomDrawingPoint.x, bottomDrawingPoint.y));
				break;




			}
			break;
		}
		case text:
		{
			CRect rect;
			rect.SetRect(point.x, point.y, point.x + 30, point.y + 20);
			CString str;

			CEdit* edit = new CEdit;
			box = FindFreeID();
			edit->Create(WS_VISIBLE | WS_CHILD | ES_MULTILINE | WS_DISABLED, rect, this, box);
			str.Format(_T("text%d"), 5);
			edit->SetWindowText(str);
			edit->SetReadOnly(true);
			rect.top += 20;
		}
		case paintBucket:
		{
			// Find out what the user clicked
			for (int i = figs.GetSize() - 1; i >= 0;i--) // Start at the end so we get the most recent figure
			{
				if (figs.GetAt(i)->IsInside(point))
				{
					figuresPaintBucketColor.push_back(std::make_pair(figs.GetAt(i), figs.GetAt(i)->insideColor));
					figs.GetAt(i)->insideColor = GetUserChosenColor();

					// Save last action
					lastActions.push_back(paintBucket);

					// Invalidate and stop looking for more figures
					InvalidateRect(CRect(topDrawingPoint.x, topDrawingPoint.y, bottomDrawingPoint.x, bottomDrawingPoint.y));
					break;
				}
			}
		}
		default:
		{
			// Do nothing
			//MessageBox(_T("Not recognized action: " + GetUserAction()));
		}

		}
	}
	UpdateOnScreenUndoAmount(lastActions.size());

	CDialogEx::OnLButtonDown(nFlags, point);
}

/// <summary>
/// Default on left mouse button up
/// </summary>
/// <param name="nFlags"></param>
/// <param name="point"></param>
void CpaintprojectDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	if ((point.y > topDrawingPoint.y))
	{
		//check if lbutton is pressed
		if (lButtonDown)
		{
			int userChosenWidth = GetUserChosenWidth();
			COLORREF userChosenColor = GetUserChosenColor();

			CClientDC dc(this);
			CPen pen(PS_SOLID, userChosenWidth, userChosenColor); // Pen <=> frame
			CPen* old = dc.SelectObject(&pen);
			CBrush brush(userChosenColor); // Brush <=> fill
			CBrush* oldB = dc.SelectObject(&brush);

			switch (GetUserAction())
			{
			case noAction:
				break;
			case freeStyle:
			case eraser:
			{
				startp = point;

				// Add -1 to our lines array as our "Ending point"
				myLine l;
				l.start = new CPoint(-1, -1);
				l.end = new CPoint(-1, -1);
				lines.push_back(l);

				linesColors.push_back(GetUserChosenColor());
				linesWidth.push_back(GetUserChosenWidth());

				//Clear screen to redraw current line
				InvalidateRect(CRect(topDrawingPoint.x, topDrawingPoint.y, bottomDrawingPoint.x, bottomDrawingPoint.y));

				// save action
				lastActions.push_back(GetUserAction());

				break;
			}
			case figure:
				endp = point;
				figs[figs.GetSize() - 1]->Redefine(startp, endp);
				InvalidateRect(CRect(topDrawingPoint.x, topDrawingPoint.y, bottomDrawingPoint.x, bottomDrawingPoint.y));
				// save action
				lastActions.push_back(figure);
				break;

			default:
			{
				// Do nothing
				//MessageBox(_T("Not recognized action: " + GetUserAction()));
			}

			}
			touched = true;

		}
	}
	//set lbutton pressed = false
	lButtonDown = FALSE;
	UpdateOnScreenUndoAmount(lastActions.size());

	CDialogEx::OnLButtonUp(nFlags, point);
}

/// <summary>
/// Default on mouse move
/// </summary>
/// <param name="nFlags"></param>
/// <param name="point"></param>
void CpaintprojectDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	if ((point.y > topDrawingPoint.y))
	{
		//check if lbutton is pressed
		if (lButtonDown)
		{
			int userChosenWidth = GetUserChosenWidth();
			COLORREF userChosenColor = GetUserChosenColor();
			CClientDC dc(this);


			switch (GetUserAction())
			{
			case noAction:
				break;
			case freeStyle:
			case eraser:
			{
				// Redfine prevs to continue adding lines
				touched = (GetUserAction() == freeStyle) ? true : false;
				CPen pen(PS_SOLID, userChosenWidth, (GetUserAction() == freeStyle) ? userChosenColor : GetSecondColor()); // Pen <=> frame
				dc.SelectObject(&pen);
				dc.MoveTo(startp.x, startp.y);
				dc.LineTo(point.x, point.y);

				// Add the line to our lines array
				myLine l;
				l.start = new CPoint(startp.x, startp.y);
				l.end = new CPoint(point.x, point.y);
				lines.push_back(l);

				linesColors.push_back((GetUserAction() == freeStyle) ? userChosenColor : GetSecondColor());
				linesWidth.push_back(GetUserChosenWidth());


				// Redfine prevs to continue adding lines
				startp = point;
				break;
			}
			case figure:
			{

				CPen pen(PS_SOLID, userChosenWidth, userChosenColor); // Pen <=> frame
				CPen* old = dc.SelectObject(&pen); // for figures
				CBrush brush(GetSecondColor()); // Brush <=> fill
				CBrush* oldB = dc.SelectObject(&brush);

				endp = point;
				figs[figs.GetSize() - 1]->Redefine(startp, endp);
				InvalidateRect(CRect(topDrawingPoint.x, topDrawingPoint.y, bottomDrawingPoint.x, bottomDrawingPoint.y));
				break;
			}
			default:
			{
				// Do nothing
				//MessageBox(_T("Not recognized action: " + GetUserAction()));
			}

			}

		}
	}
	CDialogEx::OnMouseMove(nFlags, point);
}

/// <summary>
/// Function to override keyboard keys
/// </summary>
/// <param name="pMsg"></param>
/// <returns></returns>
BOOL CpaintprojectDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_ESCAPE)
		{
			SetNone();
			return true; // Disable closing MFC
		}
		if (pMsg->wParam == VK_RETURN)
		{
			return true; // Disable closing MFC
		}
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}

/// <summary>
/// Function to set white as background
/// </summary>
/// <param name="pDC"></param>
/// <returns></returns>
BOOL CpaintprojectDlg::OnEraseBkgnd(CDC* pDC)
{
	CRect rect;
	GetClientRect(&rect);
	CBrush myBrush(RGB(255, 255, 255));    // dialog background color
	CBrush *pOld = pDC->SelectObject(&myBrush);
	BOOL bRes = pDC->PatBlt(0, 0, rect.Width(), rect.Height(), PATCOPY);
	pDC->SelectObject(pOld);    // restore old brush
	return bRes;                       // CDialog::OnEraseBkgnd(pDC);
}

void CpaintprojectDlg::SetNone()
{
	SetUserAction(noAction);
	SetCurrentFigure(noFigure);
	SetCursor(-1);
	SetDialogTitle(-1);
}

void CpaintprojectDlg::UpdateOnScreenUndoAmount(UINT newSize)
{
	CStatic* pStaticTest = (CStatic*)GetDlgItem(IDC_UNDOAMOUNT);
	std::wstringstream wss;
	std::wstring str;

	wss << newSize;
	wss >> str;
	LPCWSTR result = str.c_str();

	pStaticTest->SetWindowText(result);
}

void CpaintprojectDlg::UpdateOnScreenRedoAmount(UINT newSize)
{
	CStatic* pStaticTest = (CStatic*)GetDlgItem(IDC_REDOAMOUNT);
	std::wstringstream wss;
	std::wstring str;

	wss << newSize;
	wss >> str;
	LPCWSTR result = str.c_str();

	pStaticTest->SetWindowText(result);
}

int CpaintprojectDlg::FindFreeID()
{
	return 800;
	/*int freeID(1);
	for (;;++freeID)
	if (GetDlgItem(freeID) == 0)
	break;
	return freeID;*/
}

void CpaintprojectDlg::UndoLastLine()
{
	if (lines.size() == 0)
		return;

	// Remove -1 point
	lines.pop_back();
	linesColors.pop_back();
	linesWidth.pop_back();

	for (int i = lines.size(); i > 0; i--)
	{
		myLine curLine = lines.at(i - 1);
		if (curLine.start->x == -1)
			break;

		// Add points
		oldLines.push_back(curLine); // Add to old line array
		lines.pop_back();

		// Add color
		oldLinesColors.push_back(linesColors.at(i - 1));
		linesColors.pop_back();

		// Add width
		oldLinesWidth.push_back(linesWidth.at(i - 1));
		linesWidth.pop_back();

	}
	myLine l;
	l.start = new CPoint(-1, -1);
	l.end = new CPoint(-1, -1);
	oldLines.push_back(l);

	oldLinesColors.push_back(GetUserChosenColor());
	oldLinesWidth.push_back(GetUserChosenWidth());

	InvalidateRect(CRect(topDrawingPoint.x, topDrawingPoint.y, bottomDrawingPoint.x, bottomDrawingPoint.y));
}

void CpaintprojectDlg::RedoLastLine()
{
	if (oldLines.size() == 0)
		return;

	// Remove -1 point
	oldLines.pop_back();
	oldLinesColors.pop_back();
	oldLinesWidth.pop_back();

	for (int i = oldLines.size(); i > 0; i--)
	{
		myLine curLine = oldLines.at(i - 1);
		if (curLine.start->x == -1)
			break;

		// Add line
		lines.push_back(curLine); // Add to old line array
		oldLines.pop_back();

		// Add color
		linesColors.push_back(oldLinesColors.at(i - 1));
		oldLinesColors.pop_back();

		// Add width
		linesWidth.push_back(oldLinesWidth.at(i - 1));
		oldLinesWidth.pop_back();
	}
	myLine l;
	l.start = new CPoint(-1, -1);
	l.end = new CPoint(-1, -1);
	lines.push_back(l);

	linesColors.push_back(GetUserChosenColor());
	linesWidth.push_back(GetUserChosenWidth());

	InvalidateRect(CRect(topDrawingPoint.x, topDrawingPoint.y, bottomDrawingPoint.x, bottomDrawingPoint.y));
}

void CpaintprojectDlg::UndoLastPaintBucket()
{
	if (figuresPaintBucketColor.size() == 0)
		return;

	Figure2D *lastChangedFigure = figuresPaintBucketColor.at(figuresPaintBucketColor.size() - 1).first;
	COLORREF lastChangedFigureColor = figuresPaintBucketColor.at(figuresPaintBucketColor.size() - 1).second;

	for (int i = 0; i < figs.GetSize(); i++)
	{
		if (figs.GetAt(i) == lastChangedFigure)
		{
			oldFiguresPaintBucketColor.push_back(std::make_pair(figs.GetAt(i), figs.GetAt(i)->insideColor)); // Save current 
			figs.GetAt(i)->insideColor = lastChangedFigureColor; // Change color
			figuresPaintBucketColor.pop_back(); // Remove the pair
			break;
		}
	}

	// Invalidate
	InvalidateRect(CRect(topDrawingPoint.x, topDrawingPoint.y, bottomDrawingPoint.x, bottomDrawingPoint.y));
}

void CpaintprojectDlg::RedoLastPaintBucket()
{
	if (oldFiguresPaintBucketColor.size() == 0)
		return;

	Figure2D *lastChangedFigure = oldFiguresPaintBucketColor.at(oldFiguresPaintBucketColor.size() - 1).first;
	COLORREF lastChangedFigureColor = oldFiguresPaintBucketColor.at(oldFiguresPaintBucketColor.size() - 1).second;

	for (int i = 0; i < figs.GetSize(); i++)
	{
		if (figs.GetAt(i) == lastChangedFigure)
		{
			figuresPaintBucketColor.push_back(std::make_pair(figs.GetAt(i), figs.GetAt(i)->insideColor)); // Save current 
			figs.GetAt(i)->insideColor = lastChangedFigureColor; // Change color
			oldFiguresPaintBucketColor.pop_back(); // Remove the pair
			break;
		}
	}

	// Invalidate
	InvalidateRect(CRect(topDrawingPoint.x, topDrawingPoint.y, bottomDrawingPoint.x, bottomDrawingPoint.y));
}

void CpaintprojectDlg::UndoLastFigure()
{
	// Save figure to oldFigs
	oldFigs.Add(figs.GetAt(figs.GetSize() - 1));

	// remove figure from figs
	figs.RemoveAt(figs.GetSize() - 1);

	// Invalidate
	InvalidateRect(CRect(topDrawingPoint.x, topDrawingPoint.y, bottomDrawingPoint.x, bottomDrawingPoint.y));
}

void CpaintprojectDlg::RedoLastFigure()
{
	// Save figure to figs
	figs.Add(oldFigs.GetAt(oldFigs.GetSize() - 1));

	// remove figure from oldFigs
	oldFigs.RemoveAt(oldFigs.GetSize() - 1);

	// Invalidate
	InvalidateRect(CRect(topDrawingPoint.x, topDrawingPoint.y, bottomDrawingPoint.x, bottomDrawingPoint.y));
}

void CpaintprojectDlg::ClearScreenAndResetToDefault()
{
	touched = false;
	SetNone();

	// Reset colors
	ConColorButt1.SetColor(RGB(0, 0, 0));		// Default starting color1 is black
	SetFirstColor(RGB(0, 0, 0));
	ConColorButt2.SetColor(RGB(255, 255, 255));	// Default starting color2 is white
	SetSecondColor(RGB(255, 255, 255));
	CheckRadioButton(IDC_COLOR1RADIO, IDC_COLOR2RADIO, IDC_COLOR1RADIO); // Color 1 is chosen by default
	isFirstColor = true;

	// Reset size
	WidthSlider.SetPos(DEFAULT_WIDTH); // Default is DEFAULT_WIDTH

									   // Reset background
	bitmap = -1;
	isBgChanged = true;
	m_Choice.SetCurSel(0);

	// Empty figures array
	figs.RemoveAll();

	// Empty freestyle vectors
	lines.clear();
	oldLines.clear();
	linesColors.clear();
	oldLinesColors.clear();
	linesWidth.clear();
	oldLinesWidth.clear();

	// Empty paintbucket map
	figuresPaintBucketColor.clear();

	// Empty action vectors
	lastActions.clear();
	oldLastActions.clear();

	// Update action counts
	UpdateOnScreenUndoAmount(lastActions.size());
	UpdateOnScreenRedoAmount(oldLastActions.size());

	// Clear screen including buttons and toolbar
	InvalidateRect(CRect(0, 0, bottomDrawingPoint.x, bottomDrawingPoint.y));
}

void CpaintprojectDlg::PerformSave()
{
	if (fileName == "")
	{
		PerformSaveAs();
	}
	else
	{
		CFile file(fileName, CFile::modeCreate | CFile::modeWrite);
		CArchive ar(&file, CArchive::store);
		figs.Serialize(ar);
		ar.Close();
		file.Close();
		saved = true;
		AfxMessageBox(_T("Saved successfully! \nFile location: " + fileName), MB_ICONINFORMATION);
	}
}

void CpaintprojectDlg::PerformSaveAs()
{
	const TCHAR czFilter[] = _T("ShaharAndAviad files (*.shaharandaviad)|*.shaharandaviad|All Files (*.*)|*.*||");
	CFileDialog fDialog(FALSE, _T("ShaharAndAviad"), NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, czFilter, this);

	if (fDialog.DoModal() == IDOK)
	{
		fileName = fDialog.GetPathName();
		CFile file(fileName, CFile::modeCreate | CFile::modeWrite);
		CArchive ar(&file, CArchive::store);
		figs.Serialize(ar);
		ar.Close();
		file.Close();
		saved = true;
		AfxMessageBox(_T("Saved successfully! \nFile location: " + fileName), MB_ICONINFORMATION);
	}
}

void CpaintprojectDlg::RedfineAllFigs()
{
	for (int i = 0; i < figs.GetSize();i++)
	{
		CPoint p1 = figs.GetAt(i)->GetP1();
		CPoint p2 = figs.GetAt(i)->GetP2();
		figs.GetAt(i)->Redefine(p1, p2);
	}
}

void CpaintprojectDlg::RedrawBackground(CDC *dc)
{
	if (bitmap != -1)
	{
		CBitmap bmp;
		bmp.LoadBitmap(bitmap);
		CBrush bmpBrush(&bmp);
		dc->FillRect(CRect(topDrawingPoint.x, topDrawingPoint.y, bottomDrawingPoint.x, bottomDrawingPoint.y), &bmpBrush);
	}
	else // Draw white background
	{
		COLORREF white = RGB(255, 255, 255);
		CPen pen(PS_SOLID, 1, white); // Pen <=> frame
		CPen* old = dc->SelectObject(&pen); // for figures
		CBrush brush(white); // Brush <=> fill
		CBrush* oldB = dc->SelectObject(&brush);
		dc->Rectangle(topDrawingPoint.x, topDrawingPoint.y, bottomDrawingPoint.x, bottomDrawingPoint.y);
	}
	isBgChanged = false;
}

void CpaintprojectDlg::RedrawAllFigs(CDC* dc)
{
	for (int i = 0; i < figs.GetSize(); i++)
	{
		CPen pen(PS_SOLID, figs[i]->frameSize, figs[i]->outsideColor); // Pen <=> frame
		CPen* old = dc->SelectObject(&pen); // for figures
		CBrush brush(figs[i]->insideColor); // Brush <=> fill
		CBrush* oldB = dc->SelectObject(&brush);
		figs[i]->Draw(dc);
	}
}

void CpaintprojectDlg::RedrawAllLines(CDC *dc)
{
	for (int i = 0; i < lines.size(); i++)
	{
		myLine curLine = lines.at(i);
		if (curLine.start->x != -1)
		{
			CPen pen(PS_SOLID, linesWidth.at(i), linesColors.at(i)); // Pen <=> frame
			dc->SelectObject(&pen);
			dc->MoveTo(curLine.start->x, curLine.start->y);
			dc->LineTo(curLine.end->x, curLine.end->y);
		}
	}
}

void CpaintprojectDlg::OnBnClickedColor1radio()
{
	SetIsFirstColor(true);
}

void CpaintprojectDlg::OnBnClickedColor2radio()
{
	SetIsFirstColor(false);
}


void CpaintprojectDlg::OnBnClickedColorpicker1()
{
	SetFirstColor(ConColorButt1.GetColor());
}


void CpaintprojectDlg::OnBnClickedColorpicker2()
{
	SetSecondColor(ConColorButt2.GetColor());
}


void CpaintprojectDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	int curWidth = WidthSlider.GetPos();
	SetUserChosenWidth(curWidth);
	CDialogEx::OnHScroll(nSBCode, nPos, pScrollBar);
}


void CpaintprojectDlg::OnCbnSelchangeCombo1()
{
	int CurSel = m_Choice.GetCurSel();
	CBitmap bmp;
	// Change the bitmap according to the selection
	switch (CurSel)
	{
	default:
	case 0:
		bitmap = -1;
		break;
	case 1:
		bitmap = IDB_HIT;
		break;
	case 2:
		bitmap = IDB_AVENGERS;
		break;
	case 3:
		bitmap = IDB_LIONKING;
		break;
	case 4:
		bitmap = IDB_BART;
		break;
	case 5:
		bitmap = IDB_HARRYPOTTER;
		break;
	case 6:
		bitmap = IDB_BEACH;
		break;
	case 7:
		bitmap = IDB_LAKE;
		break;
	case 8:
		bitmap = IDB_DESERT;
		break;
	}
	InvalidateRect(CRect(topDrawingPoint.x, topDrawingPoint.y, bottomDrawingPoint.x, bottomDrawingPoint.y));
	isBgChanged = true;
}


void CpaintprojectDlg::OnBnClickedUndo()
{
	int size = lastActions.size();

	if (size == 0)
		return;

	switch (lastActions.at(lastActions.size() - 1))
	{
	case freeStyle:
		UndoLastLine();break;
	case paintBucket:
		UndoLastPaintBucket();break;
	case figure:
		UndoLastFigure();break;

	}

	oldLastActions.push_back(lastActions.at(size - 1));
	lastActions.pop_back();

	UpdateOnScreenUndoAmount(lastActions.size());
	UpdateOnScreenRedoAmount(oldLastActions.size());

}


void CpaintprojectDlg::OnBnClickedRedo()
{
	int size = oldLastActions.size();

	if (size == 0)
		return;

	switch (oldLastActions.at(size - 1))
	{
	case freeStyle:
		RedoLastLine();break;
		break;
	case paintBucket:
		RedoLastPaintBucket();break;
	case figure:
		RedoLastFigure();break;
	}

	lastActions.push_back(oldLastActions.at(size - 1));
	oldLastActions.pop_back();

	UpdateOnScreenUndoAmount(lastActions.size());
	UpdateOnScreenRedoAmount(oldLastActions.size());
}	
