#pragma once
/// <summary>
/// file	Figure2D.cpp
/// 
/// author	Shahar Azar		208427302
/// author	Aviad Partoosh	313548323
/// date	September 2020
/// </summary>
#ifndef FIGURE2D_H
#define FIGURE2D_H
#include <afxwin.h>


class Figure2D : public CObject {
protected:
	CPoint p1;
	CPoint p2;
	int KIND;

	
public:
	Figure2D();
	Figure2D(int x1, int y1, int x2, int y2, COLORREF insideColor, COLORREF outsideColor, int frameSize);
	virtual void Draw ( CDC *dc) = 0;
	virtual void Redefine(CPoint c1, CPoint c2) { p1 = c1; p2 = c2; }
	virtual CPoint GetP1() { return this->p1; }
	virtual CPoint GetP2() { return this->p2; }
	virtual bool IsInside(const CPoint &P) const;
	virtual ~Figure2D();

	virtual void Serialize(CArchive& archive)
	{
		CObject::Serialize(archive);

		if (archive.IsStoring())
		{
			archive << p1;
			archive << p2;
			archive << insideColor;
			archive << outsideColor;
			archive << frameSize;
		}
		else
		{
			archive >> p1;
			archive >> p2;
			archive >> insideColor;
			archive >> outsideColor;
			archive >> frameSize;
		}
	}

	COLORREF outsideColor;
	COLORREF insideColor;
	int frameSize;

};

class EllipseF :public Figure2D
{
	DECLARE_SERIAL(EllipseF)

protected:

public:
	EllipseF(); // Default constructor is needed for serialization
	EllipseF(int x1, int y1, int x2, int y2, COLORREF insideColor, COLORREF outsideColor, int frameSize);

	void Draw(CDC *dc)
	{
		dc->Ellipse(p1.x, p1.y, p2.x, p2.y);
	}
	virtual void Serialize(CArchive& archive)
	{
		Figure2D::Serialize(archive);
	}
};

class RectangleF : public Figure2D
{
	DECLARE_SERIAL(RectangleF)
protected:

public:
	RectangleF(); // Default constructor is needed for serialization
	RectangleF(int x1, int y1, int x2, int y2, COLORREF insideColor, COLORREF outsideColor, int frameSize);

	void Draw(CDC *dc)
	{
		dc->Rectangle(p1.x, p1.y, p2.x, p2.y);
	}
	virtual void Serialize(CArchive& archive)
	{
		Figure2D::Serialize(archive);
	}
};

class SegmentF :public Figure2D
{
	DECLARE_SERIAL(SegmentF)
protected:
public:
	SegmentF(); // Default constructor is needed for serialization
	SegmentF(int x1, int y1, int x2, int y2, COLORREF insideColor, COLORREF outsideColor, int frameSize);

	void Draw(CDC *dc)
	{
		dc->MoveTo(p1);
		dc->LineTo(p2);
		
	}
	virtual void Serialize(CArchive& archive)
	{
		Figure2D::Serialize(archive);
	}
};

class TriangleF :public Figure2D
{
	DECLARE_SERIAL(TriangleF)
protected:
	CPoint T_P[3];
public:
	TriangleF(); // Default constructor is needed for serialization
	TriangleF(int x1, int y1, int x2, int y2, COLORREF insideColor, COLORREF outsideColor, int frameSize);

	virtual void Redefine(CPoint c1, CPoint c2);
	void Draw(CDC *dc)
	{
		dc->Polygon(T_P,3);
	}
	virtual void Serialize(CArchive& archive)
	{
		Figure2D::Serialize(archive);
	}
};

class R_A_Triangle :public TriangleF
{
	DECLARE_SERIAL(R_A_Triangle)
protected:
	CPoint T_P[3];
public:
	R_A_Triangle(); // Default constructor is needed for serialization
	R_A_Triangle(int x1, int y1, int x2, int y2, COLORREF insideColor, COLORREF outsideColor, int frameSize);

	void Redefine(CPoint c1, CPoint c2);
	void Draw(CDC *dc)
	{
		dc->Polygon(T_P, 3);
	}
	virtual void Serialize(CArchive& archive)
	{
		Figure2D::Serialize(archive);
	}
};

class S_O_David : public TriangleF
{
	DECLARE_SERIAL(S_O_David)
protected:
	CPoint T_P[3];
	CPoint T_Q[3];
public:
	S_O_David(); // Default constructor is needed for serialization
	S_O_David(int x1, int y1, int x2, int y2, COLORREF insideColor, COLORREF outsideColor, int frameSize);

	void Redefine(CPoint c1, CPoint c2);
	void Draw(CDC *dc)
	{

		// Draw normal triangle
		dc->MoveTo(T_Q[0].x, T_Q[0].y);
		dc->LineTo(T_Q[1].x, T_Q[1].y);
		dc->MoveTo(T_Q[1].x, T_Q[1].y);
		dc->LineTo(T_Q[2].x, T_Q[2].y);
		dc->MoveTo(T_Q[2].x, T_Q[2].y);
		dc->LineTo(T_Q[0].x, T_Q[0].y);

		// Draw backwards triangle
		dc->MoveTo(T_P[0].x, T_P[0].y);
		dc->LineTo(T_P[1].x, T_P[1].y);
		dc->MoveTo(T_P[1].x, T_P[1].y);
		dc->LineTo(T_P[2].x, T_P[2].y);
		dc->MoveTo(T_P[2].x, T_P[2].y);
		dc->LineTo(T_P[0].x, T_P[0].y);
	}
	virtual void Serialize(CArchive& archive)
	{
		Figure2D::Serialize(archive);
	}
};
#endif