
// paint_projectDlg.h : header file
//
#include <sstream>
#include <vector>
#include "Figure2D.h"

#pragma once

enum Action { noAction, freeStyle, paintBucket, text, eraser, save, trash, figure };
enum Figure { noFigure, ellipse, rectangle, segment, isoscelesTriangle, rightTriangle, starOfDavid };

struct myLine
{
	CPoint* start;
	CPoint* end;
};

// CpaintprojectDlg dialog
class CpaintprojectDlg : public CDialogEx
{
	Figure futureFigKIND;
	Figure2D* f;
	int indexCurr;
	CTypedPtrArray< CObArray, Figure2D*> figs, oldFigs;
	int bitmap;
	CPoint startp;
	CPoint endp;
	CPoint T_P[3];
	CPoint topDrawingPoint;
	CPoint bottomDrawingPoint;

	std::vector<myLine> lines;
	std::vector<myLine> oldLines;
	std::vector<COLORREF> linesColors;
	std::vector<COLORREF> oldLinesColors;
	std::vector<int> linesWidth;
	std::vector<int> oldLinesWidth;

	std::vector<std::pair<Figure2D*, COLORREF>> figuresPaintBucketColor;
	std::vector<std::pair<Figure2D*, COLORREF>> oldFiguresPaintBucketColor;

	std::vector<Action> lastActions;
	std::vector<Action> oldLastActions;


	// Construction 
public:
	CpaintprojectDlg(CWnd* pParent = nullptr);	// standard constructor
	CpaintprojectDlg::~CpaintprojectDlg(); // destractor for the figs array
	CMFCButton* m_pToolsButtonArr[6];
	CMFCColorButton ConColorButt1;
	CMFCColorButton ConColorButt2;
	CSliderCtrl WidthSlider;
	CComboBox m_Choice;




	// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PAINT_PROJECT_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;
	COLORREF firstColor;
	COLORREF secondColor;
	Action userAction;
	char* windowTitle = "Paint";
	bool lButtonDown;
	bool touched;
	bool saved;
	bool isFirstColor;
	bool isBgChanged;
	int chosenWidth;
	int DEFAULT_WIDTH = 5;
	int box = 800;
	HBRUSH m_whitebrush;
	CString fileName;


	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	void CreateToolButtons();
	void CreateFigureButtons();
	void OnToolButtonClick(UINT);
	void OnFigureButtonClick(UINT);
	void DefineColorButtons();
	void DefineWidthSlider();
	afx_msg HCURSOR OnQueryDragIcon();
	COLORREF GetUserChosenColor();
	void SetFirstColor(COLORREF);
	COLORREF GetFirstColor();
	void SetSecondColor(COLORREF);
	COLORREF GetSecondColor();
	bool IsFirstColor();
	void SetIsFirstColor(bool);
	void SetUserChosenWidth(int);
	int GetUserChosenWidth();
	void SetUserAction(Action);
	Action GetUserAction();
	void SetCurrentFigure(Figure);
	Figure GetCurrentFigure();
	void SetCursor(UINT);
	void SetDialogTitle(UINT);
	void SetNone();
	void UpdateOnScreenUndoAmount(UINT);
	void UpdateOnScreenRedoAmount(UINT);
	int FindFreeID();
	void UndoLastLine();
	void RedoLastLine();
	void UndoLastPaintBucket();
	void RedoLastPaintBucket();
	void UndoLastFigure();
	void RedoLastFigure();
	void ClearScreenAndResetToDefault();
	void PerformSave();
	void PerformSaveAs();
	void RedfineAllFigs();
	void RedrawBackground(CDC*);
	void RedrawAllFigs(CDC*);
	void RedrawAllLines(CDC*);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void onMenuNew();
	afx_msg void OnMenuOpen();
	afx_msg void OnMenuSave();
	afx_msg void OnMenuSaveas();
	afx_msg void OnMenuAbout();
	afx_msg void OnMenuExitpaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnBnClickedColor1radio();
	afx_msg void OnBnClickedColor2radio();
	afx_msg void OnBnClickedColorpicker1();
	afx_msg void OnBnClickedColorpicker2();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnCbnSelchangeCombo1();
private:
	CBitmap HITBMP, BEACHBMP, DESERTBMP, LAKEBMP, BARTBMP, AVENGERSBMP, HARRYPOTTERBMP;
public:
	afx_msg void OnBnClickedUndo();
	afx_msg void OnBnClickedRedo();
};
