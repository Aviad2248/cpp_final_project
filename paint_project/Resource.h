//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by paintproject.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDR_MAINFRAME                   128
#define IDR_MENU1                       130
#define IDI_ERASER                      145
#define IDI_PAINT_BUCKET                147
#define IDI_PENCIL                      148
#define IDI_ICON5                       149
#define IDI_TEXT                        149
#define IDI_SAVE                        150
#define IDI_TRASH                       151
#define IDD_PAINT_PROJECT_DIALOG        154
#define IDC_CURSOR1                     155
#define IDC_CURSOR2                     156
#define IDS_TEST                        158
#define IDI_ICON1                       166
#define IDI_COLORPALETTE                166
#define IDB_HIT                         168
#define IDB_AVENGERS                    169
#define IDB_BART                        170
#define IDB_BEACH                       171
#define IDB_DESERT                      172
#define IDB_HARRYPOTTER                 173
#define IDB_LAKE                        174
#define IDI_ICON2                       175
#define IDI_ELLIPSE                     176
#define IDI_ISOSCELESTRIANGLE           177
#define IDI_RECTANGLE                   178
#define IDI_RIGHTTRIANLGE               179
#define IDI_SEGMENT                     180
#define IDI_STAROFDAVID                 181
#define IDB_BITMAP1                     182
#define IDB_LIONKING                    182
#define IDC_COLORPICKER1                1010
#define IDC_MFCCOLORBUTTON2             1011
#define IDC_COLORPICKER2                1011
#define IDC_COLOR1RADIO                 1012
#define IDC_COLOR2RADIO                 1013
#define IDC_SLIDER1                     1014
#define IDC_CHOICE                      1015
#define IDC_UNDO                        1016
#define IDC_REDO                        1017
#define IDC_UNDOAMOUNT                  1018
#define IDC_STACKSIZE2                  1019
#define IDC_REDOAMOUNT                  1019
#define ID_MENU_OPTION                  32771
#define ID_MENU_OPEN                    32772
#define ID_MENU_SAVE                    32773
#define ID_MENU_SAVEAS                  32774
#define ID_MENU_ABOUT                   32775
#define ID_MENU_EXITPAINT               32776
#define ID_NEW                          32777
#define ID_ABOUT                        32778
#define ID_ABOUT_A                      32779
#define ID_SAVEAS                       32780
#define ID_EXITPAINT                    32781
#define ID_EXITPAINT_A                  32782
#define ID_NEW_NEW                      32783
#define ID_OPEN                         32784
#define ID_SAVE                         32785
#define ID_MENU_NEW                     32791
#define ID_MEN_NEW                      32792
#define ID_FEATURES_LOADBACKGROUND      32793

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        183
#define _APS_NEXT_COMMAND_VALUE         32794
#define _APS_NEXT_CONTROL_VALUE         1019
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
